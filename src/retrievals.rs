// ISC License (ISC)
//
// Copyright (c) 2016, Austin Hellyer <hello@austinhellyer.me>
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
// SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
// RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
// CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
// CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

use curl::http;
use std::ffi::OsStr;
use std::fs::File;
use std::io::{BufReader, BufRead};
use std::str;
use walkdir::WalkDir;

// Replaces commonly-used entries with another and returns back a modified
// `String`.
fn host_cleanup(hosts: String) -> String {
    // Replace `127.0.0.1` with `0.0.0.0`. As `127.0.0.1` can be where actual
    // locally-running applications bind to, entries can be accidentally be
    // re-routed to hit these locally-running applications.
    hosts.replace("127.0.0.1", "0.0.0.0")
}

// Loops through the `data` directory and finds all of the `update.info` files
// in the `data` directory's subdirectories. These are files that contain only a
// single URL to a third-party hosts text
// file.
//
// The URLs to these text files are stored in a `Vec` and returned.
pub fn block_urls(path_to_repo: &str) -> Vec<String> {
    let folders = WalkDir::new(&format!("{}/data", path_to_repo)[..])
        .into_iter()
        .filter_map(|e| e.ok());

    let mut urls: Vec<String> = vec![];

    for entry in folders {
        // If the entry's filename isn't `update.info` then skip it, as we're
        // only interested in files named precisely that.
        if entry.path().file_name() != Some(OsStr::new("update.info")) {
            continue;
        }

        match File::open(entry.path()) {
            Ok(file) => {
                let reader = BufReader::new(file);

                // Really there should be only one line, but in the future if
                // these `update.info` files have multiple then this is a
                // safeguard.
                for line in reader.lines() {
                    let url: String = line.unwrap();

                    // If the `urls` `Vec` already contains the URL, then skip
                    // over it so as not to push it to the `Vec`.
                    if urls.contains(&url) {
                        continue;
                    }

                    urls.push(url.clone());
                }
            },
            Err(why) => panic!("Couldn't open file: {}", why),
        }
    }

    urls
}

// Loops through the `Vec` of given URLs, retrieve its contents, and then pushes
// the contents to a `String`. This `String` is then checked for any values that
// need replacing and then returned as one big `String` of the contents of the
// URLs.
pub fn hosts(urls: Vec<String>) -> String {
    let mut hosts: String = String::new();

    for url in urls {
        println!("Downloading and parsing list at: {}", url);

        let resp = http::handle()
            .get(url)
            .exec()
            .unwrap();

        let contents: &str = str::from_utf8(resp.get_body())
            .unwrap();

        hosts.push_str(contents);
    }

    host_cleanup(hosts)
}
