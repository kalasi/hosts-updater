// ISC License (ISC)
//
// Copyright (c) 2016, Austin Hellyer <hello@austinhellyer.me>
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
// SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
// RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
// CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
// CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

extern crate curl;
extern crate walkdir;

mod retrievals;

use std::fs::File;
use std::io::prelude::*;
use std::process::Command;

fn main() {
    // Filepath as a `str` of where to store the temporarily-created clone of
    // the hosts repo.
    let tmp: &str = "/tmp/hosts-upd";

    // URL to the `hosts` repo. The repo contains a `data` directory, which in
    // turn contains more directories. Each of these has a file named
    // `update.info`, whose contents is in turn a URL to a text file.
    //
    // The text file is separated by newlines, each with a hosts entry to a
    // domain.
    //
    // An entry may look like:
    //
    // ```
    // 0.0.0.0 101com.com
    // ```
    //
    // Some of these hosts text files contain entries that point to `127.0.0.1`,
    // but as this can disrupt actual locally-running applications, these are
    // repliaced with `0.0.0.0`, which is more or less essentially a void.
    let url: &str = "https://github.com/StevenBlack/hosts";

    let status = Command::new("git")
        .arg("clone")
        .arg(url)
        .arg(tmp)
        .status()
        .unwrap_or_else(|e| panic!("Failed to clone {}: {}", url, e));

    if !status.success() {
        panic!("Error cloning repo {}", url);
    }

    let urls = retrievals::block_urls(tmp);
    let hosts = retrievals::hosts(urls);

    println!("Removing temporary hosts repo");

    let status = Command::new("rm")
        .arg("-rf")
        .arg(tmp)
        .status()
        .unwrap_or_else(|e| panic!("Failed to rm {}: {}", tmp, e));

    if !status.success() {
        panic!("Error removing {}", tmp);
    }

    // Read the contents of the current `/etc/hosts` file. This is so that it
    // can be analyzed and split, saving non-automatically-generated hosts
    // entries.
    let mut f = File::open("/etc/hosts").unwrap();
    let mut s: String = String::new();

    let _ = f.read_to_string(&mut s);

    let mut final_hosts: String = String::new();

    if s.contains("# hosts-updater-start") {
        let split: Vec<&str> = s.split("# hosts-updater-start").collect();

        final_hosts.push_str(split[0]);
    } else {
        final_hosts.push_str(&s[..]);
    }

    final_hosts.push_str("# hosts-updater-start\n");
    final_hosts.push_str(&hosts[..]);
    final_hosts.push_str("# hosts-updater-end");

    if s.contains("# hosts-updater-end") {
        let split: Vec<&str> = s.split("# hosts-updater-end").collect();

        final_hosts.push_str(split[1]);
    }

    // Finally, write the new hosts file to `/etc/hosts`.
    println!("Writing the new hosts to /etc/hosts");
    let mut f_new = File::create("/etc/hosts").unwrap();
    let _ = f_new.write_all(final_hosts.as_bytes());
}
