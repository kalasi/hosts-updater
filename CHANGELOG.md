# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning][semver].

## [Unreleased]

### Added

### Changed

## [0.1.0] - 2016-03-13

Initial commit.

[Unreleased]: https://gitlab.com/kalasi/gtld-data.rs/compare/v0.1.0...master
[semver]: http://semver.org
