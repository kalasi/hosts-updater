[![ci-badge][]][ci] [![license-badge][]][license]

# hosts-updater

Rust project for automatically adding a compiled list of malicious hosts to
your hosts file.

This project works by cloning a copy of StevenBlack's `hosts` repo on GitHub,
cycling through the `data` directory to find URLs of hosts files to download,
and then compiles all of those host files to one, adding it to your `/etc/hosts`
file.

### Installation

1. `git clone https://gitlab.com/kalasi/hosts-updater.git`
2. `cd hosts-updater`
3. `cargo build --release`
4. `./target/release/hosts-updater`

### Flags

There are none. Just run the compiled binary and you're good to go.

### License

License info in [LICENSE.md]. Long story short, ISC.

[ci]: https://gitlab.com/kalasi/hosts-updater/pipelines
[ci-badge]: https://gitlab.com/kalasi/hosts-updater/badges/master/build.svg
[LICENSE.md]: https://gitlab.com/kalasi/hosts-updater/blob/master/LICENSE.md
[license]: https://opensource.org/licenses/ISC
[license-badge]: https://img.shields.io/badge/license-ISC-blue.svg?style=flat-square
